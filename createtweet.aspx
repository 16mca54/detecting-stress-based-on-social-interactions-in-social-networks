﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user.master" AutoEventWireup="true" CodeBehind="createtweet.aspx.cs" Inherits="WebApplication2.createtweet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    
    <script type="text/javascript">
        function getData()
        {
            Image1.ImageUrl = "~/Files/" + Path.GetFileName(FileUpload1.FileName);

        }
    </script>
    
    <style type="text/css">
        .d
        {
            z-index: 1;
            top: 537px;
            position: absolute;
            height: 394px;
            width: 752px;
            left: 31px;
            right: 158px;
        }
        #TextArea1 {
            height: 88px;
            width: 125px;
        }
        #emojionearea1 {
            height: 106px;
            width: 92px;
        }
        .row {
            width: 100px;
        }
        .text {}


    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="d"> 
    <asp:Label ID="Label2" runat="server" 
        style="z-index: 1; left: 78px; top: 104px; position: absolute; font-size: large; font-weight: 700; font-family: 'Times New Roman', Times, serif; color: #CC3399; width: 100px;" 
        Text="Tweet Name"></asp:Label>

        <asp:Label ID="Label6" runat="server" 
        
            style="z-index: 1; left: 484px; top: 24px; position: absolute; font-size: large; font-weight: 700; font-family: 'Times New Roman', Times, serif; color: #CC3399; width: 106px;"></asp:Label>

            <asp:Label ID="Label7" runat="server" 
        
            
            style="z-index: 1; left: 484px; top: 51px; position: absolute; font-size: large; font-weight: 700; font-family: 'Times New Roman', Times, serif; color: #CC3399; width: 106px;" 
            Visible="False"></asp:Label>
        
        <asp:DropDownList ID="DropDownList1" runat="server" 
            style="z-index: 1; left: 236px; top: 198px; position: absolute; height: 16px; width: 206px" Visible="False">
            <asp:ListItem>--Select--</asp:ListItem>
            <asp:ListItem>Positive</asp:ListItem>
            <asp:ListItem>Negative</asp:ListItem>
            <asp:ListItem>Stress</asp:ListItem>
        </asp:DropDownList>

         <%--<asp:DropDownList ID="DropDownList2" runat="server" placeholder="Emojis" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged"
            style="z-index: 1; left: 464px; top: 98px; position: absolute; height: 30px; width: 88px; " Font-Size="Medium">
            <asp:ListItem>Emoji</asp:ListItem>
            <asp:ListItem>☺</asp:ListItem>
            <asp:ListItem>☹</asp:ListItem>
            <asp:ListItem>☀</asp:ListItem>
             
        </asp:DropDownList>--%>

    <asp:Button ID="Button4" runat="server" 
        style="z-index: 1; left: 254px; top: 291px; position: absolute; width: 81px; color: #CC3399; font-size: large; font-weight: 700; font-family: 'Times New Roman', Times, serif; " 
        Text="Post" onclick="Button4_Click" />
    <asp:Label ID="Label3" runat="server" 
        style="z-index: 1; left: 78px; top: 151px; position: absolute; font-size: large; font-weight: 700; font-family: 'Times New Roman', Times, serif; color: #CC3399; width: 157px; height: 21px;" 
        Text="Tweet Description"></asp:Label>
    <asp:Label ID="Label4" runat="server" 
        style="z-index: 1; left: 77px; top: 192px; position: absolute; font-size: large; font-weight: 700; font-family: 'Times New Roman', Times, serif; color: #CC3399; width: 104px;" 
        Text="Tweet Type" Visible="False"></asp:Label>
    <asp:Label ID="Label1" runat="server" 
        style="z-index: 1; left: 166px; top: 32px; position: absolute; width: 274px; color: #000000; font-size: x-large; font-weight: 700; font-family: 'Times New Roman', Times, serif" 
        Text="POSTING TWEETS...."></asp:Label> 

        

    <asp:Button AutoPostBack="true" ID="Button5" runat="server" 
        style="z-index: 1; left: 471px; top: 95px; position: absolute; width: 40px; color: #CC3399; font-size: small; font-weight: 700; font-family: 'Times New Roman', Times, serif; height: 24px; right: 271px; " 
        Text="Emoji" Font-Size="Smaller"  OnClick="Button5_Click"/>



    <asp:FileUpload ID="FileUpload1" runat="server" 
        style="z-index: 1; left: 236px; top: 240px; position: absolute; width: 189px; height: 22px;" OnDataBinding="FileUpload1_DataBinding1"  />
               
    <%--<asp:Button ID="Button2" runat="server" 
        style="z-index: 1; left: 431px; top: 241px; position: absolute; width: 38px; color: #CC3399; font-size: large; font-weight: 700; font-family: 'Times New Roman', Times, serif; height: 25px; bottom: 128px;" 
        Text="OK" type="button"  Font-Size="Smaller" />--%>

       <%--<input type = "button" name="data" onclick="getData()" value="Click">--%>
        <asp:TextBox ID="TextBox5" runat="server"
        style="z-index: 1; left: 350px; top: 293px; position: absolute; width: 111px" Visible="False"></asp:TextBox> 


         <asp:Panel AutoPostBack="true" ID="Panel1" runat="server" CssClass="coin-slider" BackColor="White" Visible="False">
            <asp:Button ID="la1" runat="server"  Text="☹" Font-Bold="True" Font-Size="X-Large" Height="38px" Width="33px" OnClick="la1_Click" AutoPostBack="True" BackColor="White" BorderColor="White" BorderStyle="None" ></asp:Button>
            <asp:Button ID="la2" runat="server" Text="☺" Height="39px" Width="30px" AutoPostBack="True" BackColor="White" BorderColor="White" Font-Bold="True" Font-Size="X-Large" BorderStyle="None" OnClick="la2_Click"></asp:Button>
             <asp:Button ID="la3" runat="server" Text="☠" Height="39px" Width="30px" AutoPostBack="True" BackColor="White" BorderColor="White" Font-Bold="True" Font-Size="X-Large" BorderStyle="None" OnClick="la3_Click"></asp:Button>
        </asp:Panel>


    <asp:Label ID="Label5" runat="server" 
        style="z-index: 1; left: 77px; top: 236px; position: absolute; font-size: large; font-weight: 700; font-family: 'Times New Roman', Times, serif; color: #CC3399; width: 153px;" 
        Text="Select Tweet Image"></asp:Label>
        
    <asp:TextBox ID="TextBox3" runat="server" placeholder="Type a message here"
        style="z-index: 1; top: 147px; position: absolute; width: 206px; right: 304px;"></asp:TextBox>                        
             
        <asp:Image ID="Image1" runat="server" 
            style="z-index: 1; left: 470px; top: 240px; position: absolute; width: 112px; height: 98px;"  />
        

    </div>
    

    <asp:TextBox ID="TextBox1" runat="server"
               style="z-index: 1; left: 266px; top: 635px; position: absolute; width: 200px"></asp:TextBox>
        

         


        
</asp:Content>


