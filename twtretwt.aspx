﻿<%@ Page Title="" Language="C#" MasterPageFile="~/server.master" AutoEventWireup="true" CodeBehind="twtretwt.aspx.cs" Inherits="WebApplication2.twtretwt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .g
    {
        z-index: 1;
        left: 25px;
        top: 560px;
        position: absolute;
        height: 749px;
        width: 703px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="g" style="overflow: scroll">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" 
        CellPadding="4" 
        EmptyDataText="There are no data records to display." 
        
            style="z-index: 1; left: 3px; top: 53px; position: absolute; height: 559px; width: 691px; font-size: medium; font-family: 'Times New Roman', Times, serif;" 
            CellSpacing="2" ForeColor="Black">
        <Columns>
            <asp:BoundField DataField="Tweet_Name" HeaderText="Tweet_Name" 
                SortExpression="Tweet_Name" />
            <asp:BoundField DataField="Tweet_Description" HeaderText="Tweet_Description" 
                SortExpression="Tweet_Description" />
            <asp:BoundField DataField="Tweet_By" HeaderText="Tweet_By" 
                SortExpression="Tweet_By" />
            <asp:BoundField DataField="Tweet_Type" HeaderText="Tweet_Type" 
                SortExpression="Tweet_Type" />
            <asp:BoundField DataField="Retweet_By" HeaderText="Retweet_By" 
                SortExpression="Retweet_By" />
            <asp:BoundField DataField="Retweet" HeaderText="Retweet" 
                SortExpression="Retweet" />
            <asp:TemplateField HeaderText="Image">
                 <ItemTemplate>
                       <asp:Image ID ="Img" runat="server" Height="100" Width="100"   ImageUrl='<%#Eval("Image")%>'  />
                 </ItemTemplate>
             </asp:TemplateField>
          <%--  <asp:ImageField DataImageUrlField="Image" HeaderText="Image" ControlStyle-Height="100" ControlStyle-Width="100">
            </asp:ImageField>--%>
        </Columns>
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
        <RowStyle BackColor="White" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />
    </asp:GridView>
       <%-- <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:stressdetectConnectionString %>" 
            SelectCommand="SELECT [Tweet_Name], [Tweet_Description], [Tweet_By], [Tweet_Type], [Image], [Retweet_By], [Retweet] FROM [createtweet]">
        </asp:SqlDataSource>--%>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
