﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace WebApplication2
{
    public partial class stressemo : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection("Data Source=USER-PC\\SQLEXPRESS;Initial Catalog=stressdetect;User ID=sa;Password=123");
        SqlCommand cm = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            con.Open();
            // da = new SqlDataAdapter("select Tweet_Name,postive from createtweet where Tweet_Type = 'Stress'", con);
            da = new SqlDataAdapter("select Tweet_By+' / '+Tweet_Name as 'Tweet' ,stress from createtweet where stress > 0", con);
            dt = new DataTable();
            da.Fill(dt);
            Chart1.DataSource = dt;
            Chart1.DataBind();
            con.Close();
        }
    }
}