﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user.master" AutoEventWireup="true" CodeBehind="twtcmt.aspx.cs" Inherits="WebApplication2.twtcmt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .c
        {
            z-index: 1;
            left: 4px;
            top: 492px;
            position: absolute;
            height: 700px;
            width: 733px;
            
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="c" style="overflow: scroll">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" 
        CellPadding="4"
        EmptyDataText="There are no data records to display." 
        
            style="z-index: 1; left: 7px; top: 43px; position: absolute; height: 571px; width: 636px; font-family: 'Times New Roman', Times, serif; font-size: medium;" 
            CellSpacing="2" ForeColor="Black">
        <Columns>
            <asp:BoundField DataField="TweetName" HeaderText="TweetName" 
                SortExpression="TweetName" />
            <asp:BoundField DataField="TweetDescription" HeaderText="TweetDescription" 
                SortExpression="TweetDescription" />
            <asp:TemplateField HeaderText="Image">
                 <ItemTemplate>
                       <asp:Image ID ="Img" runat="server" Height="100" Width="100"   ImageUrl='<%#Eval("Image")%>'  />
                 </ItemTemplate>
             </asp:TemplateField>
            <asp:BoundField DataField="RetweetBy" HeaderText="RetweetBy" 
                SortExpression="RetweetBy" />
            <asp:BoundField DataField="Retweet" HeaderText="Retweet" 
                SortExpression="Retweet" />
            <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" />
        </Columns>
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
        <RowStyle BackColor="White" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />
    </asp:GridView>
        <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:stressdetectConnectionString %>" 
            SelectCommand="SELECT [TweetName], [TweetDescription], [RetweetBy], [Retweet], [TweetType], [Date] FROM [retweet]">
        </asp:SqlDataSource>--%>
    </div>
</asp:Content>
