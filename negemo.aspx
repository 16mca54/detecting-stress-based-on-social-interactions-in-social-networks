﻿<%@ Page Title="" Language="C#" MasterPageFile="~/server.master" AutoEventWireup="true" CodeBehind="negemo.aspx.cs" Inherits="WebApplication2.negemo" %>
<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .l
        {
            z-index: 1;
            left: 224px;
            top: 550px;
            position: absolute;
            height: 351px;
            width: 671px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="l">

        <asp:Chart ID="Chart1" runat="server">
            <series>
                <asp:Series Name="Series1"  XValueMember="Tweet" YValueMembers="negative">
                </asp:Series>
            </series>
            <chartareas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </chartareas>
        </asp:Chart>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:stressdetectConnectionString %>" 
        SelectCommand="SELECT [Tweet_Name], [negative] FROM [createtweet]">
    </asp:SqlDataSource>

</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
