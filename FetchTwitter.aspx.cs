﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Security.Cryptography;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Data;

using System.Text.RegularExpressions;
using System.Collections;
namespace WebApplication2
{
    public partial class FetchTwitter : System.Web.UI.Page
    {
        public string query = "@AnithaRenjit";
        //public string query = "/1.1/statuses/user_timeline.json?screen_name=AnithaRenjit";
        //public string url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
        public string url = "https://api.twitter.com/1.1/statuses/home_timeline.json";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                findUserTwitter(url, query);
            }
        }
        public void findUserTwitter(string resource_url, string q)
        {
            // oauth application keys
            var oauth_token = "965445553428418560-mdfgJUdYOpmx967cukDIwqHg2kCvZaz"; //"insert here...";
            var oauth_token_secret = "INam7V0IlWhanpksJWU2C9kisH30mNhgjwxzdmUnJZ4bA"; //"insert here...";
            var oauth_consumer_key = "aNA4Z3JgGChmpBwiYfknEjxbb";// = "insert here...";
            var oauth_consumer_secret = "h5Li6NsNvEWoheLWHAZowRUnwIo6PdjawIf87Z4SFxdvf27LOg";// = "insert here...";

            // oauth implementation details
            var oauth_version = "1.0";
            var oauth_signature_method = "HMAC-SHA1";

            // unique request details
            var oauth_nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            var timeSpan = DateTime.UtcNow
                - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();


            // create oauth signature
            var baseFormat = "oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
                            "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}&q={6}";

            var baseString = string.Format(baseFormat,
                                        oauth_consumer_key,
                                        oauth_nonce,
                                        oauth_signature_method,
                                        oauth_timestamp,
                                        oauth_token,
                                        oauth_version,
                                        Uri.EscapeDataString(q)
                                        );

            baseString = string.Concat("GET&", Uri.EscapeDataString(resource_url), "&", Uri.EscapeDataString(baseString));

            var compositeKey = string.Concat(Uri.EscapeDataString(oauth_consumer_secret),
                                    "&", Uri.EscapeDataString(oauth_token_secret));

            string oauth_signature;
            using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
            {
                oauth_signature = Convert.ToBase64String(
                    hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
            }

            // create the request header
            var headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
                               "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
                               "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
                               "oauth_version=\"{6}\"";

            var authHeader = string.Format(headerFormat,
                                    Uri.EscapeDataString(oauth_nonce),
                                    Uri.EscapeDataString(oauth_signature_method),
                                    Uri.EscapeDataString(oauth_timestamp),
                                    Uri.EscapeDataString(oauth_consumer_key),
                                    Uri.EscapeDataString(oauth_token),
                                    Uri.EscapeDataString(oauth_signature),
                                    Uri.EscapeDataString(oauth_version)
                            );
            ServicePointManager.Expect100Continue = false;

            // make the request
            var postBody = "q=" + Uri.EscapeDataString(q);//
            resource_url += "?" + postBody;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
            request.Headers.Add("Authorization", authHeader);
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            var response = (HttpWebResponse)request.GetResponse();
            var reader = new StreamReader(response.GetResponseStream());
            var objText = reader.ReadToEnd();
            //myDiv.InnerHtml = objText;/**/
            //TextBox1.Text = objText;

            string html = "";
            try
            {
                JArray jsonDat = JArray.Parse(objText);
                for (int x = 0; x < jsonDat.Count(); x++)
                {
                    //html += jsonDat[x]["id"].ToString() + "<br/>";
                    html += jsonDat[x]["text"].ToString() + "<br/>";
                    //html += jsonDat[x]["name"].ToString() + "<br/>";
                    //html += jsonDat[x]["created_at"].ToString() + "<br/>";

                    TextBox1.Text += "*" + (x + 1) + "*" + Environment.NewLine;
                    TextBox1.Text += "ID= " + jsonDat[x]["id"].ToString() + Environment.NewLine;
                    TextBox1.Text += jsonDat[x]["text"].ToString() + Environment.NewLine;
                }
                //myDiv.InnerHtml = html;


            }
            catch (Exception twit_error)
            {
                //myDiv.InnerHtml = html + twit_error.ToString();
                TextBox1.Text = html + twit_error.ToString();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=USER-PC\\SQLEXPRESS;Initial Catalog=stressdetect;User ID=sa;Password=123");
            SqlCommand cm = new SqlCommand();
            try
            {
                int n1, n2, n3;
                string s1, s2;
                n1 = int.Parse(TextBox2.Text);
                n2 = n1 + 1;
                s1 = "*" + n1.ToString() + "*"; s2 = "*" + n2.ToString() + "*";

                //---------------------------------    UPDATE POS,NEG,STRS   ----------------------------------------------
                string st = TextBox1.Text;
                string str = st.Substring(st.IndexOf(s1), st.LastIndexOf(s2) - st.IndexOf(s1));
                string[] arr = str.Split(' ');

                int pos = 0;
                int neg = 0;
                int strs = 0;

                int x = Regex.Matches(str, "☺").Count;// +Regex.Matches(str, "").Count;
                int y = Regex.Matches(str, "☹️").Count;
                int z = Regex.Matches(str, "☀️").Count;

                if (x > 0) { pos = pos + x; }
                if (y > 0) { neg = neg + y; }
                if (z > 0) { strs = strs + z; }

                for (int i = 0; i < arr.Length; i++)
                {
                    Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                    arr[i] = rgx.Replace(arr[i], "");

                    con.Open();
                    SqlCommand cmb = new SqlCommand();
                    cmb = new SqlCommand("select * from positive where Fillter = '" + arr[i] + "'", con);
                    SqlDataAdapter da = new SqlDataAdapter(cmb);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        pos = pos + ds.Tables[0].Rows.Count;
                    }
                    con.Close();

                    con.Open();
                    SqlCommand cmv = new SqlCommand();
                    cmv = new SqlCommand("select * from negative where Fillter ='" + arr[i] + "'", con);
                    SqlDataAdapter da1 = new SqlDataAdapter(cmv);
                    DataSet ds1 = new DataSet();
                    da1.Fill(ds1);
                    if (ds1.Tables[0].Rows.Count > 0)
                    {
                        neg = neg + ds1.Tables[0].Rows.Count;
                    }
                    con.Close();

                    con.Open();
                    SqlCommand cmc = new SqlCommand();
                    cmc = new SqlCommand("select * from stress where Fillter ='" + arr[i] + "'", con);
                    SqlDataAdapter da2 = new SqlDataAdapter(cmc);
                    DataSet ds2 = new DataSet();
                    da2.Fill(ds2);
                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        strs = strs + ds2.Tables[0].Rows.Count;
                    }
                    con.Close();
                }

                if ((pos > 0) || (neg > 0) || (strs > 0))
                {
                    if (pos > neg)
                    {
                        if (pos > strs)
                        {
                            TextBox3.Text = "Positive";
                        }
                        else
                        {
                            TextBox3.Text = "Stress";
                        }

                    }
                    else if (neg > pos)
                    {
                        if (neg > strs)
                        {
                            TextBox3.Text = "Negative";
                        }
                        else
                        {
                            TextBox3.Text = "Stress";
                        }
                    }
                    else
                    {
                        TextBox3.Text = "Can't Determine";

                    }
                }
                else
                {
                    TextBox3.Text = "Can't Determine";
                }
            }
            catch (Exception ex) { Response.Write(ex.Message); }
        }

        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}