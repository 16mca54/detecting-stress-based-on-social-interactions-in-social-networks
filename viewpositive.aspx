﻿<%@ Page Title="" Language="C#" MasterPageFile="~/server.master" AutoEventWireup="true" CodeBehind="viewpositive.aspx.cs" Inherits="WebApplication2.viewpositive" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .i
        {
            z-index: 1;
            left: -125px;
            top: 646px;
            position: absolute;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="i" style="overflow: scroll; margin-bottom: 2px;">

        </div>

    <asp:Label ID="Label1" runat="server" Text="View Positive Tweets" 
        style="color: #000000; font-size: medium; font-weight: 700; font-family: 'Times New Roman', Times, serif; z-index: 1; left: 379px; top: -120px; position: absolute; width: 197px; text-transform: uppercase; height: 21px"></asp:Label>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        BackColor="#CC3399" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" 
        CellPadding="4" CellSpacing="2"  ForeColor="Black" 
        
        
        style="z-index: 1; left: 93px; top: 582px; position: absolute; height: 670px; width: 674px; color: #000000; font-size: medium; font-family: 'Times New Roman', Times, serif; background-color: #FFFFFF; margin-right: 0px;">
        <Columns>
           
            <asp:BoundField  DataField="Tweet_Name" HeaderText="Tweet_Name" 
                SortExpression="Tweet_Name" />
            <asp:BoundField  DataField="Tweet_Description" HeaderText="Tweet_Description" 
                SortExpression="Tweet_Description" />
            <asp:BoundField  DataField="Tweet_By" HeaderText="Tweet_By" 
                SortExpression="Tweet_By" />
            <asp:TemplateField HeaderText="Image">
                 <ItemTemplate>
                       <asp:Image ID ="Img" runat="server" Height="100" Width="100"   ImageUrl='<%#Eval("Image")%>'  />
                 </ItemTemplate>
             </asp:TemplateField>
           <%-- <asp:BoundField  DataField="Image" HeaderText="Image" 
                SortExpression="Image" />--%>
            <asp:BoundField  DataField="Retweet_By" HeaderText="Retweet_By" 
                SortExpression="Retweet_By" />
            <asp:BoundField  DataField="Retweet" HeaderText="Retweet" 
                SortExpression="Retweet" />
            <asp:BoundField  DataField="Date" HeaderText="Date" 
                SortExpression="Date" />
           
        </Columns>
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Wheat" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
        <RowStyle BackColor="White" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />
    </asp:GridView>





</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
