﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmojiForm.aspx.cs" Inherits="WebApplication2.EmojiForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Twitter Emotions</title>
</head>
    <script src="//twemoji.maxcdn.com/twemoji.min.js"></script>
<script src="jquery-1.10.2.min.js"></script>
<script src="TEmoji.js"></script>


<body>
    <form id="form1" runat="server">
    
        <asp:Label ID="LblTwitterEmojiMsg" runat="server" CssClass="lbltwitteremojimsg"></asp:Label>
        <div class="container">
            <div class="emojibox">
                <asp:TextBox ID="TxtEmoji" runat="server" CssClass="textemoji" placeholder="Type a message here"></asp:TextBox>
                <input type="button" id="BtnEmoji" class="btnemoji" value="&#x1F600;" />
                <ul class="emoji-list">
                    <li>&#x1F332;</li>
                    <li>&#x1F333;</li>
                    <li>&#x1F334;</li>
                    <li>&#x1F335;</li>
                    <li>&#x1F337;</li>
                    <li>&#x1F338;</li>
                    <li>&#x1F339;</li>
                    <li>&#x1F33A;</li>
                    <li>&#x1F33B;</li>
                    <li>&#x1F33C;</li>
                    <li>&#x1F33D;</li>
                    <li>&#x1F33E;</li>
                    <li>&#x1F33F;</li>
                    <li>&#x1F340;</li>
                    <li>&#x1F341;</li>
                    <li>&#x1F342;</li>
                    <li>&#x1F343;</li>
                    <li>&#x1F344;</li>
                    <li>&#x1F345;</li>
                    <li>&#x26EA;</li>
                </ul>
            </div>
            <asp:Button ID="BtnSendEmoji" runat="server" Text="Send" CssClass="sendbutton" OnClick="BtnSendEmoji_Click" />
        </div>
    </form>    
</body>
</html>
